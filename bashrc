# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Control bash history
# http://www.biostat.jhsph.edu/~afisher/ComputingClub/webfiles/KasperHansenPres/IntermediateUnix.pdf
# https://unix.stackexchange.com/questions/48713/how-can-i-remove-duplicates-in-my-bash-history-preserving-order
# don't put duplicate lines or lines starting with space in the history.
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000

shopt -s histappend
shopt -s cmdhist
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Change the up and down arrows
# Auto-complete command from history
# http://lindesk.com/2009/04/customize-terminal-configuration-setting-bash-cli-power-user/
# export INPUTRC=~/.inputrc
# "\e[B": history-search-forward
# "\e[A": history-search-backward

 bind '"\e[A": history-search-backward'
# bind '"\e0A": history-previous-history'
 bind '"\e[B": history-search-forward'
# bind '"\e0B": history-next-history'

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# Change command prompt
# http://www.cyberciti.biz/tips/howto-linux-unix-bash-shell-setup-prompt.html
# http://www.cyberciti.biz/faq/bash-shell-change-the-color-of-my-shell-prompt-under-linux-or-unix/
# https://bbs.archlinux.org/viewtopic.php?id=48910
# previous in enigma2: "[\u@\h \W]\$ "
# previously in mac: "\h:\W \u\$ "
export PS1="\[\e[0;33m\]\A \W \$ \[\e[m\]"

source ~/.aliases

export PATH=$PATH:~/.local/bin
